import React from 'react';
import { Component } from 'react';
import { Row, Col, Grid, Panel } from 'react-bootstrap';
import SideNavigation from '../containers/side_navigation';
import ActiveItem from '../containers/active_item';
import LoginButton from '../containers/login_btn';
import Header from '../containers/header';

export default class App extends Component {
  render() {
    return (
      <Grid fluid={true} className='fill'>
        <Row className='fill'>
          <Col md={2} className='side-nav no-padding'>
            <LoginButton />
            <SideNavigation />
          </Col>
          <Col md={10} className='main-container no-padding'>
            <Header />
            <ActiveItem />
          </Col>
        </Row>
      </Grid>
    );
  }
}
