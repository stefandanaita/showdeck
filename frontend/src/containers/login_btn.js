import React, {Component} from 'react';

export default class LoginButton extends Component {
  render() {
    return (
      <button
        type="button"
        className="login-btn">
        LOG IN
      </button>
    )
  }
}
