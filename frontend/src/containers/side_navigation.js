import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Glyphicon} from 'react-bootstrap';
import {selectItem} from '../actions/index';

class SideNavigation extends Component {

  renderNavigation() {

    var itemSelected = this.props.active_item?true:false;

    return this.props.nav_items.map((nav_item) => {
      var activeMarker = itemSelected && this.props.active_item.title == nav_item.title?'active-item':null;
      return (
        <button
          type="button"
          className={"list-group-item nav-item "+activeMarker}
          key={nav_item.title}
          onClick={() => this.props.selectItem(nav_item)}>
          <strong><Glyphicon glyph={nav_item.icon} /> {nav_item.title}</strong>
        </button>
      );
    });

  }

  render () {
    return (
      <div className="list-group">
        {this.renderNavigation()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    nav_items: state.nav_items,
    active_item: state.active_item
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ selectItem: selectItem }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SideNavigation);
