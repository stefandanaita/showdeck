import React, {Component} from 'react';
import {connect} from 'react-redux';

class ShowSearch extends Component {

  constructor(props) {
    super(props);
    this.state = { term: '' };
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(event) {
    //console.log(event.target.value);
    this.setState({ term: event.target.value });
  }

  render() {
    return (
      <input
        type="text"
        placeholder="Search for a show..."
        value={this.state.term}
        className="search-bar clearfix"
        onChange={this.onInputChange} />
    );
  }
}

export default connect(null, null)(ShowSearch);
