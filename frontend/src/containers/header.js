import React, {Component} from 'react';
import { Row, Col } from 'react-bootstrap';
import ShowSearch from './show_search';

export default class Header extends Component {
  render() {
    return (
      <Row className='header-panel clearfix vertical-align'>
        <Col xs={2} className='notification-icons clearfix'>
          icons
        </Col>
        <Col xs={8} className='search-container'>
          <ShowSearch />
        </Col>
        <Col xs={2} className="logout-btn clearfix">
          Logout
        </Col>
      </Row>
    )
  }
}
