import React, {Component} from 'react';
import {connect} from 'react-redux';

class ActiveItem extends Component {

  render() {

    if(!this.props.nav_item)  {
      return <div>Dashboard</div>;
    }

    return (
      <span>{this.props.nav_item.title}</span>
    );
  }
}

function mapStateToProps(state) {
  return {
    nav_item: state.active_item
  };
}

export default connect(mapStateToProps)(ActiveItem);
