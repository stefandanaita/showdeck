export default function() {
  return [
    { title: 'Dashboard', icon: 'record align-left'},
    { title: 'Calendar', icon: 'calendar align-left'},
    { title: 'Trending Shows', icon: 'signal align-left'},
    { title: 'Discover', icon: 'th align-left'}
  ]
}
