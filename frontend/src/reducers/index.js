import { combineReducers } from 'redux';
import NavItems from './reducer_nav_items';
import ActiveItem from './reducer_active_item';

const rootReducer = combineReducers({
  nav_items: NavItems,
  active_item: ActiveItem
});

export default rootReducer;
