export function selectItem(nav_item) {
  return {
    type: 'ITEM_SELECTED',
    payload: nav_item
  };
}
